import sys
import os
import importlib

def set_syspath(absolute_dependancies: list):
	# absolute_dependancies : une liste de noms absolutes de sossiers
	for directory in absolute_dependancies:
		# On vérifie l'éxistance du dossier
		existe = os.path.exists(directory) and os.path.isdir(directory)

		if not existe:
			# On lève une exception
			raise BaseException(f"[set_syspath] le dossier du python path [{directory}] n'existe pas ")
		else:
			# On ajoute le dossier au début du syspath
			sys.path.insert(0,directory)


def getfiles(directory):
	""" Retourner la liste des noms des fichiers contenus dans 'directory'  """
	if os.path.exists(directory) :
		folders = os.listdir(directory);
		i = len(folders) - 1;

		while i >= 0 :
			if  os.path.isdir(folders[i]) :
				del folders[i];

			i -= 1;

		return folders;


def isPythonFile(filename):
    """ Retourne True s'il s'agit d'un script python, False sinon. """

    return filename[-3:] == '.py';


def getModuleName(filename):
	""" Retourne le nom du fichier sans l'extension '.py'."""

	return filename[:-3];


def auto_import_from_dir(directory: str):
	""" Importe en module python tous les fichiers contenus dans directory """
	files = getfiles(directory);

	for file in files :
	    if isPythonFile(file) :
		    importlib.import_module(getModuleName(file));


def auto_import_from_dirs(directories: list):
    for directory in directories :
        auto_import_from_dir(directory);


#print(getModuleName(getfiles("./api/controllers")[0]));
