from configs.config import configure
import os
# on configure l'application avec les données du projet :
 # on recupere le dossier la racine du dossier du projet qu'on passe en argument  :
app = configure(script_dir=os.path.dirname(os.path.abspath(__file__)));

# on crée les tables avec leurs configurations dans les différentes bases de données :
#app.config['database0']['metadata'].create_all(app.config['database0']['engine']);

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0');
