#!/usr/bin/python3

import os
import sys
import argparse as agp

# liste des versions diponibles de CFlasks :
VERSIONS = [
    '1.0.0',
    '1.1.0',
    '2.0.0',
];

# initialisation du parseur d'arguments :
parser = agp.ArgumentParser();
parser.add_argument('-w', type=str, default='./', help="Pour spécifier le dossier du projet.");
parser.add_argument('-v','--version', type=str, choices=VERSIONS, default='2.0.0', help="Pour spécifier la version de CFlasks à initialiser.");

if __name__ == '__main__' :
    argv      = parser.parse_args();
    directory = argv.w;
    version   = argv.version;

  #  print(argv);
 # 
    if directory != './' :
        if os.path.isdir(f"{directory}") :
            print(f"{directory} est un dossier existant.");
            rep = input('Écraser le contenu? o/n --> ');

            if rep == 'n' :
                sys.exit();

            else :
                os.system(f"rm -r {directory}");

        os.system(f"mkdir {directory}");
        os.chdir(f"{directory}");


#	os.system('cp  -r /var/initflask/* .');
    os.system(f'tar -xJvf /var/initflask/initflask-{version}.tar.xz -C .');
    os.system('mv initflask/* .');
    os.system('rm -r initflask/');
