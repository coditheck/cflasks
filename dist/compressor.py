#!/usr/bin/python3

import os

files = [
    'requirements',
    'config.py',
    'config_databases.py',
    'config_layers.py',
    'config_tools.py',
    'main',
    'run.py',
];




elemstxt = ' '.join([('../' + file) for file in files]);
os.system('tar -cJvf initflask-2.0.0.tar.xz ' + elemstxt);
