
from config_tools import set_syspath


class App:
    """  la classe [App] represente une sous application de l'application WEB. """

    # liste des noms des dossiers d'une application :
    DIRECTORIES = [
        "entities",
        "interfaces",
        "dao",
        "utils",
        "controllers",
    ];

    def __init__(self, appname, script_dir):
        self.__script_dir = script_dir;
        self.__appname    = appname;


    def getPaths(self, idx=None):
        """ Retourne le chemin du dossiers [idx] ou tous les dossiers de l'application """

        if not idx : return [f'{self.__script_dir}/{self.__appname}/{dirn}' for dirn in App.DIRECTORIES];
        else       : return f'{self.__script_dir}/{self.__appname}/{App.DIRECTORIES[idx]}';



# END App


def configure(script_dir):
    # script_dir est le full path de la racine du projet
    
	# liste des sous applications du projet :
    apps = [
	    App('main', script_dir),
    ];

    # liste des dependences a integrer dans le PYTHON PATH :
    absolute_dependences = [];

    # on ajoute tous les dossiers des sous applications dans la liste [absolute_dependences] :
    for app in apps:
        absolute_dependences.extend(app.getPaths());

    # dictionnaire des configurations globales :
    config = {};

	# liste de tous les dossiers contenant les controlleurs de chaque sous applications :
    config['CONTROLLERS'] = [app.getPaths(4) for app in apps];

    # 
    set_syspath(absolute_dependences);
    set_syspath([f"{script_dir}/configs"]);

    # ces importations doivent se mettre apres l'appel de la fonction set_syspath() :
    import config_databases
    import config_layers

	# configuration de ou des bases de donnees :
    config = config_databases.configure(config);

	# configuration des layers :
    fapp = config_layers.configure(config);

    return fapp;

