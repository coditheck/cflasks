from flask import Flask
import secrets
from config_tools import auto_import_from_dirs

# l'application WEB Flask :
app = None;

def configure(config):
    # [app] doit etre une variable globale :
    global app;

	# on instancie l'application web :
    app = Flask(__name__);

	# configuration de la clé secret de flask 

    app.secret_key = secrets.token_urlsafe(32);

	# importation des controllers
    auto_import_from_dirs(config["CONTROLLERS"]);

    # on sauvegarde les configurations dans le dictionnaire de configuration de l'application :
    app.config.update(config);

    return app;
