# les objets de base de donnees :
from sqlalchemy 	import Table
from sqlalchemy 	import Column
from sqlalchemy 	import MetaData

# les type de colonnes dans les tables :
from sqlalchemy 	import Integer
from sqlalchemy 	import String
from sqlalchemy 	import DateTime

# les outils de relations entre tables :
from sqlalchemy.orm	import relationship
from sqlalchemy 	import ForeignKey 

# les outils d'administration de donnees de base de donnes :
from sqlalchemy 	import create_engine
from sqlalchemy.orm	import sessionmaker
from sqlalchemy.orm import mapper

# Importation des entités
#from ClassA 		import ClassA
#from ClassB        import ClassB

"""
    Il peut arriver qu'on ait envie de créer pleusieurs base de donnees 
    pour une application WEB. Pour cela, il faut les avoir cree au paravant.
    Ensuite modifier ce fichier [config_databases.py] pour mettre en place la
    structure des tables pour chacune des bases de donnees et creer une session
    pour chacune d'entre elles.

"""


def configure(config):

	# ##############################################################################
	#                                                                              #
	#   CONNECTION AUX DIFFERENTES BASES DE DONNEES                                #
	#                                                                              #
	# ##############################################################################

	# les données de connexion a la base de données sont renseignées :
	#   + username  : nom d'utilisateur utilisées dans la SGBDR;
	#   + password  : mot de passe;  
	#   + localhost : si la base de données est locale;
	#   + 5432      : est le port d'écoute de la SGBDR postgreSQL;
	#   + dbname    : le nom de la base de données.
#	engine0  = create_engine("postgresql://username:password@localhost:5432/dbname");

	# le système se sert des metadatas pour pouvoir créer les tables :
#	metadata0 = MetaData();





	# ##############################################################################
	#                                                                              #
	#   STRUCTURE ET MAPPAGE DES TABLES ET RELATIONS ENTRE ELLES                   #
	#                                                                              #
	# ##############################################################################

    # dictionnaire des tables de la base de données [0] :
#	tables_db0 = {};

	# Creation des tables
	# tables['TableA'] = A = \
	# Table("TableA", metadata,
	# 	Column("id", Integer,       primary_key=True),
	# 	Column("label", String(16), nullable=False)
	# );
	#
	# mapper(ClassA, A, properties={
	# 	"id" 	: A.c.id,
	# 	"label"	: A.c.label
	# });
	#
	# tables['TableB'] = B = \
	# Table("TableB", metadata,
	# 	Column("id", Integer, primary_key=True),
	# 	Column("classA_id", ForeignKey("TableA.id")),
	# 	Column("type", String(1), nullable=False, default='s')
	# );
	#
	# mapper(ClassB, B, properties={
	# 	"id"		: B.c.id,
	# 	"classA"   	: relationship(ClassA,    backref="ClassB_menbers",  lazy="select"),
	# 	"type"		: B.c.type
	# });
	#
	#
	# # configuration des entitees mappees :
	# # on exclut l'attribut [_sa_instance_state] créer par sqlalchemy et les backref (facultatif)
	# ClassA.excluded_keys    = ['_sa_instance_state'];
	# # Pour les tables qui contiennent des cles etrangeres, il faut exclure en plus
	# # les attributs representant les cles etrangeres.
	# ClassB.excluded_keys = ['_sa_instance_state', 'ClassB_menbers'];







    # ##############################################################################
    #                                                                              #
    #   CREATION DES SESSIONS SUR LES BASES DE DONNEES                             #
    #                                                                              #
    # ##############################################################################

	# create session factory :
#	Session0 = sessionmaker();
#	Session0.configure(bind=engine0);

	# session instance :
#	session0 = Session0();

	# stockage des donnees de bases de donnees dans le dictonnaire de config generale :
#	config['database0'] = {
#        "engine"    : engine0, 
#        "metadata"  : metadata0, 
#        "session"   : session0, 
#        "tables"    : tables_db0,
#    };
#	config['database1'] = {
#        "engine"    : engine1, 
#        "metadata"  : metadata1, 
#        "session"   : session1, 
#        "tables"    : tables_db1,
#    };

	return config;
