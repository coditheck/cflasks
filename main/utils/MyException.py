class MyException(Exception):
	"""Exception class for manager project exception"""
	def __init__(self, code: int, message: str):
		BaseException.__init__(self, message)

		# error code
		self.code = code
		self.message = message

	def __str__(self):
		return f"MyException[{self.code}, {super().__str__()}] "

	# getter
	@property
	def code(self) -> int:
		return self._code

	# setter 
	@code.setter
	def code(self,code: int):
		# required an integer positive

		if isinstance(code, int) and code > 0:
			self._code = code

		else:
			# raise an exception 

			raise BaseException(f"code error {code} incorrect")
			
	

		