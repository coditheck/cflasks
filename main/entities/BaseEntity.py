# imports
import json
import re

from MyException import MyException

class BaseEntity(object):
	"""class extends by some class encapsulate"""
	# Une liste des clés exclues 
	excluded_keys = []

	# returne la liste des cles autorisees a apparaitre dans les ressources
	@staticmethod
	def get_allowed_keys() -> list:
		"""return allowed keys list """
		return ["id", 'created', 'updated']


	# to string
	def __str__(self) -> str:
		return self.asjson()

	# id getter
	@property
	def id(self):
		return self.__id


	@property
	def created(self):
		return self.__created;


	@property
	def updated(self):
		return self.__updated;

	# setter
	@id.setter
	def id(self, id):
		self.__id = id


	@created.setter
	def created(self, value):
		self.__created = value;


	@updated.setter
	def updated(self, value):
		self.__updated = value;


	def fromdict(self, state: dict, silent=False):
		# receive allowed keys
		allowed_keys = self.__class__.get_allowed_keys()
		# initialise object attribut allowed
		for key, value in state.items():
			if key not in allowed_keys:
				if not silent:
					raise MyException(2, f"{key} is not allowed ")
			else:
				setattr(self,key,value)
		return self

	def asdict(self,included_keys: list = None, excluded_keys: list =[]) -> dict:
		""" return on dict special attribute:value"""
		attributes = self.__dict__
		return_dict = {}
		# On parcours le dictionnaire des attributs et valeurs 
		for key, value in attributes.items():
			# On vérifie si l'actuelle clé est pas manuellement incluse puis on l'ajoute
			vkey= BaseEntity.visible_attr(key)
			if included_keys and vkey in included_keys:
				self.set_value(vkey,value,return_dict)
			# Sinon on vérifie si l'actuelle clé n'est pas une clé non exclue par défaut ou 
			# si elle n'est pas excluse manuellement puis on l'ajoute
			elif not included_keys and vkey not in self.__class__.excluded_keys and vkey not in excluded_keys:
				self.set_value(vkey,value,return_dict)
		# On return un dictoonnaire comportant les clés inclusent manuellement ou non excluse 
		# manuellement et n'etant pas éxclusent par défaut
		return return_dict

	@staticmethod
	def visible_attr(attr) :
		# On vérifie si l'attribut  est  sous la forme de __Class__key
		match = re.match("^.*?__(.*?)$", attr)
		if match:
			visibleAttr = match.groups()[0]

		else:
			visibleAttr = attr
		return visibleAttr

	@staticmethod
	def set_value(key: str, value, addAs: dict):
		addAs[BaseEntity.visible_attr(key)] = BaseEntity.check_value(value)
	
	@staticmethod
	def check_value(value):
		# La valeur peut être de type BaseEntity, list, ou une type simple 
		# On traite selon le type de value
		if isinstance(value, BaseEntity) :
			returned = value.asdict()
		elif isinstance(value, list) :
			returned = BaseEntity.list2list(value)
		elif isinstance(value, dict):
			returned = BaseEntity.dict2dict(value)
		else:
			returned = value

		# On returne la valeur formaté en cas de néccessité
		return returned
	# Utiliser quand un atribut est une liste afin de vérifier tout les éléments contenu
	@staticmethod
	def list2list(data: list) -> list:
		# On inspecte les éléments de la liste. 
		newlist = []
		for value in data:
			newlist.append(BaseEntity.check_value(value))				
		return newlist

	@staticmethod
	def dict2dict(data: dict) -> dict:
		newdict = {}
		for key, value in data.items():
			newdict[key] = BaseEntity.check_value(value)
		return newdict
	
	def asjson(self, included_keys: list = None, excluded_keys: list = []) ->str:
		return json.dumps(self.asdict(included_keys=included_keys, excluded_keys=excluded_keys),ensure_ascii=False)
		
	def fromjson(self, json_state: str, silent:bool = False):
		""" Met ajour l'objet en se servant d'une chaine json"""
		return self.fromdict(json.loads(json_state),silent=silent)
		
if __name__ == '__main__':
	b= BaseEntity()
	b.id = 55
	b.autor = {"ent":55,"why":"new","list":["element 1",2,85],'id':25}
	c = BaseEntity().fromdict(b.autor,silent=True)
	print(c.asdict())
	print(b)
