from dao_interface import DAOInterface

class DAO(DAOInterface):

	def __init__(self, session):
		self.__session = session;



	def post(self, value):
		# add  ajoute a la session 
		self.__session.add(value);
		# Valide l'ajoute sans effacer l'objet ajouter en session
		self.__session.flush();



	def rollback(self):
		# Efface les derniers ajout présent dans la session
		if self.__session:
			self.__session.rollback();



	def commit(self):
		# Valider la session et efface tout les objets ajouter avec succès
		self.__session.commit();
		

# end