from abc import ABC, abstractmethod


class DAOInterface(ABC):

	@abstractmethod
	def post(self, value):
		pass;

	@abstractmethod
	def rollback(self):
		pass;

	@abstractmethod
	def commit(self):
		pass;



# end